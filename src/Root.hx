import starling.display.Sprite;
import starling.utils.AssetManager;
import starling.display.Image;
import starling.core.Starling;
import starling.animation.Transitions;

class Root extends Sprite {

    public static var assets:AssetManager;

    public function new() {
        super();
    }
    public function initialize(startup:Startup) {
        assets = new AssetManager();
        // enqueue here
        assets.enqueue("assets/loading.png");
        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup.loadingBitmap,
                    1.0,
                    {
                        transition: Transitions.EASE_OUT,
                        delay: 1.0,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
                        }
                    });
                start();
            }
        });
    }

    public function start(){
        trace("Begin!");
    }
}
